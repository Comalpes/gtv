Traversée du Vercors d’Autrans à Aouste sur Sye (26) :\
120 km en 3 jours, +3470m / -4908m en mode VTT All Mountain.\
Ça descend plus que ça monte ;-)\
Je connais une partie du parcours, il y a du roulant, du moins roulant, de beaux singles et surtout de très beaux paysages.

J1 - Crêtes de la Molière > Refuge de la Jasse du Play\
39,73 km, +1113m / -1140m

J2 - > Refuge de Tubanet\
41,22 km, +1369m / -1662m

J3 - > Aouste sur Sye\
39,06 km, +988m / -2105m

Les refuges ne sont pas gardés, normalement on trouve de l’eau à proximité.

![Alt text](GTV_j1s.jpg "J1")

![Alt text](GTV_j2s.jpg "J2")

![Alt text](GTV_j3s.jpg "J3")
